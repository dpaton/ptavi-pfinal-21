#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente que abre un socket a un servidor
"""

import socket
import sys
import socketserver
import time
import threading
from recv_rtp import RTPHandler

try:
    SERVER = 'localhost'
    IPServSIP = str(sys.argv[1].split(':')[0])
    PORTServSIP = int(sys.argv[1].split(':')[1])
    dirCliente = sys.argv[2]
    dirServRTP = sys.argv[3]
    tiempo = sys.argv[4]
    fichero = sys.argv[5]

except IndexError:
        print("Usage: python3 client.py <IPServerSIP>:<portServerSIP> "
              "<addrClient> <addrServerRTP> <time> <file>")
tiempoo = time.strftime("%Y%m%d%H%M%S", time.gmtime())


def main():
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.connect((SERVER, PORTServSIP))
        ip = my_socket.getsockname()[0]
        puerto = my_socket.getsockname()[1]
        direccion = ip + " " + str(puerto)
        peticion = "REGISTER " + dirCliente +\
                   " SIP/2.0" + "\r\n" + direccion + "\r\n"
        mensaje = peticion.split("\r\n")[0]
        print(f"{tiempoo} SIP to {ip}:{puerto} {mensaje}.")
        my_socket.sendto(bytes(peticion, 'utf-8'), (SERVER, PORTServSIP))
        data = my_socket.recv(2048)
        sip = data.decode('utf-8')
        respuesta = sip.split("\r\n")[0]

        if respuesta == "SIP/2.0 200 OK":
            print(f"{tiempoo} SIP from {ip}:{puerto} {respuesta}.")
            peticion = "INVITE " + dirServRTP + " SIP/2.0" + "\r\n" +\
                       "Content-Type: application/sdp" +\
                       "\r\n\r\nv=0\r\no=yo@clientes.net 127.0.0.1" \
                                                         "\r\ns=heyjude\r\nt=0\r\nm=audio 34543 RTP\r\n"
            mensaje = peticion.split("\r\n")[0]
            print(f"{tiempoo} SIP to {ip}:{puerto} {mensaje}.")
            my_socket.sendto(bytes(peticion, 'utf-8'), (SERVER, PORTServSIP))
            data2 = my_socket.recv(2048)
            sip2 = data2.decode('utf-8')
            respuesta2 = sip2.split("\r\n")[0]

            if respuesta2 == "SIP/2.0 200 OK":

                RTPHandler.open_output(fichero)
                try:

                    with socketserver.UDPServer((SERVER,
                                                 0), RTPHandler) as serv:
                        puerto = serv.server_address[1]
                        ipp = serv.server_address[0]
                        dir = ipp + " " + str(puerto)
                        print(dir)
                        peticion = "ACK " + dirServRTP +\
                                   " SIP/2.0" + "\r\n" + dir + "\r\n"
                        mensaje = peticion.split("\r\n")[0]
                        print(f"{tiempoo} SIP to {ip}:{puerto} {mensaje}.")
                        my_socket.sendto(bytes(peticion, 'utf-8'),
                                         (SERVER, PORTServSIP))
                        print(f"RTP ready {puerto}")
                        threading.Thread(target=serv.serve_forever).start()
                        time.sleep(10)
                        peticion2 = "BYE " + dirServRTP + " SIP/2.0" + "\r\n"
                        mensaje = peticion2.split("\r\n")[0]
                        print(f"{tiempoo} SIP to {ip}:{puerto} {mensaje}.")
                        my_socket.sendto(bytes(peticion2, 'utf-8'),
                                         (SERVER, PORTServSIP))
                        print("Time passed, shutting down receiver loop.")
                        serv.shutdown()
                    RTPHandler.close_output()
                except ConnectionRefusedError:
                    print("Error de conexion")





if __name__ == "__main__":
    main()
